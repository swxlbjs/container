#docker  build -t 172.17.6.34:3687/club/nginx:v1 -f 1.df  .
#docker run -it -d  172.17.6.34:3687/club/nginx:v1

FROM nginx:latest
MAINTAINER swxlbjs 410041390@qq.com
RUN ln -sf /usr/share/zoneinfo/Asia/Shanghai /etc/localtime
RUN echo 'Asia/Shanghai' >/etc/timezone
COPY conf.d/* /etc/nginx/conf.d/
CMD ["/usr/sbin/nginx","-g", "daemon off;"]
